/*
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package uk.co.beacon.customerservice.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.beacon.customerservice.client.model.Link;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import uk.co.beacon.customerservice.client.common.JSON;

/**
 * EntityModelVehicle
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen")
public class EntityModelVehicle {
  public static final String SERIALIZED_NAME_IMO_NUMBER = "imoNumber";
  @SerializedName(SERIALIZED_NAME_IMO_NUMBER)
  private String imoNumber;

  public static final String SERIALIZED_NAME_NAME = "name";
  @SerializedName(SERIALIZED_NAME_NAME)
  private String name;

  public static final String SERIALIZED_NAME_LINKS = "_links";
  @SerializedName(SERIALIZED_NAME_LINKS)
  private Map<String, Link> links = null;

  public EntityModelVehicle() { 
  }

  public EntityModelVehicle imoNumber(String imoNumber) {
    
    this.imoNumber = imoNumber;
    return this;
  }

   /**
   * Get imoNumber
   * @return imoNumber
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getImoNumber() {
    return imoNumber;
  }


  public void setImoNumber(String imoNumber) {
    this.imoNumber = imoNumber;
  }


  public EntityModelVehicle name(String name) {
    
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public EntityModelVehicle links(Map<String, Link> links) {
    
    this.links = links;
    return this;
  }

  public EntityModelVehicle putLinksItem(String key, Link linksItem) {
    if (this.links == null) {
      this.links = new HashMap<>();
    }
    this.links.put(key, linksItem);
    return this;
  }

   /**
   * Get links
   * @return links
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public Map<String, Link> getLinks() {
    return links;
  }


  public void setLinks(Map<String, Link> links) {
    this.links = links;
  }



  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EntityModelVehicle entityModelVehicle = (EntityModelVehicle) o;
    return Objects.equals(this.imoNumber, entityModelVehicle.imoNumber) &&
        Objects.equals(this.name, entityModelVehicle.name) &&
        Objects.equals(this.links, entityModelVehicle.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(imoNumber, name, links);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EntityModelVehicle {\n");
    sb.append("    imoNumber: ").append(toIndentedString(imoNumber)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


  public static HashSet<String> openapiFields;
  public static HashSet<String> openapiRequiredFields;

  static {
    // a set of all properties/fields (JSON key names)
    openapiFields = new HashSet<String>();
    openapiFields.add("imoNumber");
    openapiFields.add("name");
    openapiFields.add("_links");

    // a set of required properties/fields (JSON key names)
    openapiRequiredFields = new HashSet<String>();
  }

 /**
  * Validates the JSON Object and throws an exception if issues found
  *
  * @param jsonObj JSON Object
  * @throws IOException if the JSON Object is invalid with respect to EntityModelVehicle
  */
  public static void validateJsonObject(JsonObject jsonObj) throws IOException {
      if (jsonObj == null) {
        if (EntityModelVehicle.openapiRequiredFields.isEmpty()) {
          return;
        } else { // has required fields
          throw new IllegalArgumentException(String.format("The required field(s) %s in EntityModelVehicle is not found in the empty JSON string", EntityModelVehicle.openapiRequiredFields.toString()));
        }
      }

      Set<Entry<String, JsonElement>> entries = jsonObj.entrySet();
      // check to see if the JSON string contains additional fields
      for (Entry<String, JsonElement> entry : entries) {
        if (!EntityModelVehicle.openapiFields.contains(entry.getKey())) {
          throw new IllegalArgumentException(String.format("The field `%s` in the JSON string is not defined in the `EntityModelVehicle` properties. JSON: %s", entry.getKey(), jsonObj.toString()));
        }
      }
      if (jsonObj.get("imoNumber") != null && !jsonObj.get("imoNumber").isJsonPrimitive()) {
        throw new IllegalArgumentException(String.format("Expected the field `imoNumber` to be a primitive type in the JSON string but got `%s`", jsonObj.get("imoNumber").toString()));
      }
      if (jsonObj.get("name") != null && !jsonObj.get("name").isJsonPrimitive()) {
        throw new IllegalArgumentException(String.format("Expected the field `name` to be a primitive type in the JSON string but got `%s`", jsonObj.get("name").toString()));
      }
  }

  public static class CustomTypeAdapterFactory implements TypeAdapterFactory {
    @SuppressWarnings("unchecked")
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
       if (!EntityModelVehicle.class.isAssignableFrom(type.getRawType())) {
         return null; // this class only serializes 'EntityModelVehicle' and its subtypes
       }
       final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);
       final TypeAdapter<EntityModelVehicle> thisAdapter
                        = gson.getDelegateAdapter(this, TypeToken.get(EntityModelVehicle.class));

       return (TypeAdapter<T>) new TypeAdapter<EntityModelVehicle>() {
           @Override
           public void write(JsonWriter out, EntityModelVehicle value) throws IOException {
             JsonObject obj = thisAdapter.toJsonTree(value).getAsJsonObject();
             elementAdapter.write(out, obj);
           }

           @Override
           public EntityModelVehicle read(JsonReader in) throws IOException {
             JsonObject jsonObj = elementAdapter.read(in).getAsJsonObject();
             validateJsonObject(jsonObj);
             return thisAdapter.fromJsonTree(jsonObj);
           }

       }.nullSafe();
    }
  }

 /**
  * Create an instance of EntityModelVehicle given an JSON string
  *
  * @param jsonString JSON string
  * @return An instance of EntityModelVehicle
  * @throws IOException if the JSON string is invalid with respect to EntityModelVehicle
  */
  public static EntityModelVehicle fromJson(String jsonString) throws IOException {
    return JSON.getGson().fromJson(jsonString, EntityModelVehicle.class);
  }

 /**
  * Convert an instance of EntityModelVehicle to an JSON string
  *
  * @return JSON string
  */
  public String toJson() {
    return JSON.getGson().toJson(this);
  }
}

