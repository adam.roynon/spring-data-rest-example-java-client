package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.beacon.customerservice.Link;
import uk.co.beacon.customerservice.PageMetadata;
import uk.co.beacon.customerservice.PagedModelEntityModelTransportEmbedded;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PagedModelEntityModelTransport
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class PagedModelEntityModelTransport {

  @JsonProperty("_embedded")
  private PagedModelEntityModelTransportEmbedded embedded;

  @JsonProperty("_links")
  @Valid
  private Map<String, Link> links = null;

  @JsonProperty("page")
  private PageMetadata page;

  public PagedModelEntityModelTransport embedded(PagedModelEntityModelTransportEmbedded embedded) {
    this.embedded = embedded;
    return this;
  }

  /**
   * Get embedded
   * @return embedded
  */
  @Valid 
  @Schema(name = "_embedded", required = false)
  public PagedModelEntityModelTransportEmbedded getEmbedded() {
    return embedded;
  }

  public void setEmbedded(PagedModelEntityModelTransportEmbedded embedded) {
    this.embedded = embedded;
  }

  public PagedModelEntityModelTransport links(Map<String, Link> links) {
    this.links = links;
    return this;
  }

  public PagedModelEntityModelTransport putLinksItem(String key, Link linksItem) {
    if (this.links == null) {
      this.links = new HashMap<>();
    }
    this.links.put(key, linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @Valid 
  @Schema(name = "_links", required = false)
  public Map<String, Link> getLinks() {
    return links;
  }

  public void setLinks(Map<String, Link> links) {
    this.links = links;
  }

  public PagedModelEntityModelTransport page(PageMetadata page) {
    this.page = page;
    return this;
  }

  /**
   * Get page
   * @return page
  */
  @Valid 
  @Schema(name = "page", required = false)
  public PageMetadata getPage() {
    return page;
  }

  public void setPage(PageMetadata page) {
    this.page = page;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PagedModelEntityModelTransport pagedModelEntityModelTransport = (PagedModelEntityModelTransport) o;
    return Objects.equals(this.embedded, pagedModelEntityModelTransport.embedded) &&
        Objects.equals(this.links, pagedModelEntityModelTransport.links) &&
        Objects.equals(this.page, pagedModelEntityModelTransport.page);
  }

  @Override
  public int hashCode() {
    return Objects.hash(embedded, links, page);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PagedModelEntityModelTransport {\n");
    sb.append("    embedded: ").append(toIndentedString(embedded)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

