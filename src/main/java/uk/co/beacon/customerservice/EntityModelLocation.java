package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.beacon.customerservice.Link;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * EntityModelLocation
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class EntityModelLocation {

  @JsonProperty("name")
  private String name;

  @JsonProperty("locode")
  private String locode;

  @JsonProperty("_links")
  @Valid
  private Map<String, Link> links = null;

  public EntityModelLocation name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  */
  
  @Schema(name = "name", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public EntityModelLocation locode(String locode) {
    this.locode = locode;
    return this;
  }

  /**
   * Get locode
   * @return locode
  */
  
  @Schema(name = "locode", required = false)
  public String getLocode() {
    return locode;
  }

  public void setLocode(String locode) {
    this.locode = locode;
  }

  public EntityModelLocation links(Map<String, Link> links) {
    this.links = links;
    return this;
  }

  public EntityModelLocation putLinksItem(String key, Link linksItem) {
    if (this.links == null) {
      this.links = new HashMap<>();
    }
    this.links.put(key, linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @Valid 
  @Schema(name = "_links", required = false)
  public Map<String, Link> getLinks() {
    return links;
  }

  public void setLinks(Map<String, Link> links) {
    this.links = links;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EntityModelLocation entityModelLocation = (EntityModelLocation) o;
    return Objects.equals(this.name, entityModelLocation.name) &&
        Objects.equals(this.locode, entityModelLocation.locode) &&
        Objects.equals(this.links, entityModelLocation.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, locode, links);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EntityModelLocation {\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    locode: ").append(toIndentedString(locode)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

