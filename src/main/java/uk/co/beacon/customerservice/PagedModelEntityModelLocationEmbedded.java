package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import uk.co.beacon.customerservice.EntityModelLocation;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PagedModelEntityModelLocationEmbedded
 */

@JsonTypeName("PagedModelEntityModelLocation__embedded")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class PagedModelEntityModelLocationEmbedded {

  @JsonProperty("locations")
  @Valid
  private List<EntityModelLocation> locations = null;

  public PagedModelEntityModelLocationEmbedded locations(List<EntityModelLocation> locations) {
    this.locations = locations;
    return this;
  }

  public PagedModelEntityModelLocationEmbedded addLocationsItem(EntityModelLocation locationsItem) {
    if (this.locations == null) {
      this.locations = new ArrayList<>();
    }
    this.locations.add(locationsItem);
    return this;
  }

  /**
   * Get locations
   * @return locations
  */
  @Valid 
  @Schema(name = "locations", required = false)
  public List<EntityModelLocation> getLocations() {
    return locations;
  }

  public void setLocations(List<EntityModelLocation> locations) {
    this.locations = locations;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PagedModelEntityModelLocationEmbedded pagedModelEntityModelLocationEmbedded = (PagedModelEntityModelLocationEmbedded) o;
    return Objects.equals(this.locations, pagedModelEntityModelLocationEmbedded.locations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(locations);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PagedModelEntityModelLocationEmbedded {\n");
    sb.append("    locations: ").append(toIndentedString(locations)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

