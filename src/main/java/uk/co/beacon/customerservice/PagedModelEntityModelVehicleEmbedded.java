package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import uk.co.beacon.customerservice.EntityModelVehicle;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PagedModelEntityModelVehicleEmbedded
 */

@JsonTypeName("PagedModelEntityModelVehicle__embedded")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class PagedModelEntityModelVehicleEmbedded {

  @JsonProperty("vehicles")
  @Valid
  private List<EntityModelVehicle> vehicles = null;

  public PagedModelEntityModelVehicleEmbedded vehicles(List<EntityModelVehicle> vehicles) {
    this.vehicles = vehicles;
    return this;
  }

  public PagedModelEntityModelVehicleEmbedded addVehiclesItem(EntityModelVehicle vehiclesItem) {
    if (this.vehicles == null) {
      this.vehicles = new ArrayList<>();
    }
    this.vehicles.add(vehiclesItem);
    return this;
  }

  /**
   * Get vehicles
   * @return vehicles
  */
  @Valid 
  @Schema(name = "vehicles", required = false)
  public List<EntityModelVehicle> getVehicles() {
    return vehicles;
  }

  public void setVehicles(List<EntityModelVehicle> vehicles) {
    this.vehicles = vehicles;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PagedModelEntityModelVehicleEmbedded pagedModelEntityModelVehicleEmbedded = (PagedModelEntityModelVehicleEmbedded) o;
    return Objects.equals(this.vehicles, pagedModelEntityModelVehicleEmbedded.vehicles);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vehicles);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PagedModelEntityModelVehicleEmbedded {\n");
    sb.append("    vehicles: ").append(toIndentedString(vehicles)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

