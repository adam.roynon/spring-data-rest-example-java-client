package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import uk.co.beacon.customerservice.EntityModelTransportCall;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PagedModelEntityModelTransportCallEmbedded
 */

@JsonTypeName("PagedModelEntityModelTransportCall__embedded")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class PagedModelEntityModelTransportCallEmbedded {

  @JsonProperty("transportCalls")
  @Valid
  private List<EntityModelTransportCall> transportCalls = null;

  public PagedModelEntityModelTransportCallEmbedded transportCalls(List<EntityModelTransportCall> transportCalls) {
    this.transportCalls = transportCalls;
    return this;
  }

  public PagedModelEntityModelTransportCallEmbedded addTransportCallsItem(EntityModelTransportCall transportCallsItem) {
    if (this.transportCalls == null) {
      this.transportCalls = new ArrayList<>();
    }
    this.transportCalls.add(transportCallsItem);
    return this;
  }

  /**
   * Get transportCalls
   * @return transportCalls
  */
  @Valid 
  @Schema(name = "transportCalls", required = false)
  public List<EntityModelTransportCall> getTransportCalls() {
    return transportCalls;
  }

  public void setTransportCalls(List<EntityModelTransportCall> transportCalls) {
    this.transportCalls = transportCalls;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PagedModelEntityModelTransportCallEmbedded pagedModelEntityModelTransportCallEmbedded = (PagedModelEntityModelTransportCallEmbedded) o;
    return Objects.equals(this.transportCalls, pagedModelEntityModelTransportCallEmbedded.transportCalls);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transportCalls);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PagedModelEntityModelTransportCallEmbedded {\n");
    sb.append("    transportCalls: ").append(toIndentedString(transportCalls)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

