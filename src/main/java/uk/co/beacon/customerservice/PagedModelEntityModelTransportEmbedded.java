package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import uk.co.beacon.customerservice.EntityModelTransport;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PagedModelEntityModelTransportEmbedded
 */

@JsonTypeName("PagedModelEntityModelTransport__embedded")
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class PagedModelEntityModelTransportEmbedded {

  @JsonProperty("transports")
  @Valid
  private List<EntityModelTransport> transports = null;

  public PagedModelEntityModelTransportEmbedded transports(List<EntityModelTransport> transports) {
    this.transports = transports;
    return this;
  }

  public PagedModelEntityModelTransportEmbedded addTransportsItem(EntityModelTransport transportsItem) {
    if (this.transports == null) {
      this.transports = new ArrayList<>();
    }
    this.transports.add(transportsItem);
    return this;
  }

  /**
   * Get transports
   * @return transports
  */
  @Valid 
  @Schema(name = "transports", required = false)
  public List<EntityModelTransport> getTransports() {
    return transports;
  }

  public void setTransports(List<EntityModelTransport> transports) {
    this.transports = transports;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PagedModelEntityModelTransportEmbedded pagedModelEntityModelTransportEmbedded = (PagedModelEntityModelTransportEmbedded) o;
    return Objects.equals(this.transports, pagedModelEntityModelTransportEmbedded.transports);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transports);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PagedModelEntityModelTransportEmbedded {\n");
    sb.append("    transports: ").append(toIndentedString(transports)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

