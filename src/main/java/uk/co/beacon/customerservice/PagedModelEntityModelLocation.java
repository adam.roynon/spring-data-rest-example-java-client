package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.beacon.customerservice.Link;
import uk.co.beacon.customerservice.PageMetadata;
import uk.co.beacon.customerservice.PagedModelEntityModelLocationEmbedded;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * PagedModelEntityModelLocation
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class PagedModelEntityModelLocation {

  @JsonProperty("_embedded")
  private PagedModelEntityModelLocationEmbedded embedded;

  @JsonProperty("_links")
  @Valid
  private Map<String, Link> links = null;

  @JsonProperty("page")
  private PageMetadata page;

  public PagedModelEntityModelLocation embedded(PagedModelEntityModelLocationEmbedded embedded) {
    this.embedded = embedded;
    return this;
  }

  /**
   * Get embedded
   * @return embedded
  */
  @Valid 
  @Schema(name = "_embedded", required = false)
  public PagedModelEntityModelLocationEmbedded getEmbedded() {
    return embedded;
  }

  public void setEmbedded(PagedModelEntityModelLocationEmbedded embedded) {
    this.embedded = embedded;
  }

  public PagedModelEntityModelLocation links(Map<String, Link> links) {
    this.links = links;
    return this;
  }

  public PagedModelEntityModelLocation putLinksItem(String key, Link linksItem) {
    if (this.links == null) {
      this.links = new HashMap<>();
    }
    this.links.put(key, linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @Valid 
  @Schema(name = "_links", required = false)
  public Map<String, Link> getLinks() {
    return links;
  }

  public void setLinks(Map<String, Link> links) {
    this.links = links;
  }

  public PagedModelEntityModelLocation page(PageMetadata page) {
    this.page = page;
    return this;
  }

  /**
   * Get page
   * @return page
  */
  @Valid 
  @Schema(name = "page", required = false)
  public PageMetadata getPage() {
    return page;
  }

  public void setPage(PageMetadata page) {
    this.page = page;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PagedModelEntityModelLocation pagedModelEntityModelLocation = (PagedModelEntityModelLocation) o;
    return Objects.equals(this.embedded, pagedModelEntityModelLocation.embedded) &&
        Objects.equals(this.links, pagedModelEntityModelLocation.links) &&
        Objects.equals(this.page, pagedModelEntityModelLocation.page);
  }

  @Override
  public int hashCode() {
    return Objects.hash(embedded, links, page);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PagedModelEntityModelLocation {\n");
    sb.append("    embedded: ").append(toIndentedString(embedded)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

