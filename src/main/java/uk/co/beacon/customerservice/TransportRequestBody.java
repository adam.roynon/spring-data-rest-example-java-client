package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.UUID;
import uk.co.beacon.customerservice.Vehicle;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * TransportRequestBody
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class TransportRequestBody {

  @JsonProperty("id")
  private UUID id;

  @JsonProperty("vehicle")
  private Vehicle vehicle;

  /**
   * Gets or Sets mode
   */
  public enum ModeEnum {
    OCEAN("OCEAN");

    private String value;

    ModeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ModeEnum fromValue(String value) {
      for (ModeEnum b : ModeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("mode")
  private ModeEnum mode;

  @JsonProperty("arrivalPortCallId")
  private UUID arrivalPortCallId;

  @JsonProperty("destinationPortCallId")
  private UUID destinationPortCallId;

  public TransportRequestBody id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @Valid 
  @Schema(name = "id", required = false)
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public TransportRequestBody vehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
    return this;
  }

  /**
   * Get vehicle
   * @return vehicle
  */
  @Valid 
  @Schema(name = "vehicle", required = false)
  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public TransportRequestBody mode(ModeEnum mode) {
    this.mode = mode;
    return this;
  }

  /**
   * Get mode
   * @return mode
  */
  
  @Schema(name = "mode", required = false)
  public ModeEnum getMode() {
    return mode;
  }

  public void setMode(ModeEnum mode) {
    this.mode = mode;
  }

  public TransportRequestBody arrivalPortCallId(UUID arrivalPortCallId) {
    this.arrivalPortCallId = arrivalPortCallId;
    return this;
  }

  /**
   * Get arrivalPortCallId
   * @return arrivalPortCallId
  */
  @Valid 
  @Schema(name = "arrivalPortCallId", required = false)
  public UUID getArrivalPortCallId() {
    return arrivalPortCallId;
  }

  public void setArrivalPortCallId(UUID arrivalPortCallId) {
    this.arrivalPortCallId = arrivalPortCallId;
  }

  public TransportRequestBody destinationPortCallId(UUID destinationPortCallId) {
    this.destinationPortCallId = destinationPortCallId;
    return this;
  }

  /**
   * Get destinationPortCallId
   * @return destinationPortCallId
  */
  @Valid 
  @Schema(name = "destinationPortCallId", required = false)
  public UUID getDestinationPortCallId() {
    return destinationPortCallId;
  }

  public void setDestinationPortCallId(UUID destinationPortCallId) {
    this.destinationPortCallId = destinationPortCallId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransportRequestBody transportRequestBody = (TransportRequestBody) o;
    return Objects.equals(this.id, transportRequestBody.id) &&
        Objects.equals(this.vehicle, transportRequestBody.vehicle) &&
        Objects.equals(this.mode, transportRequestBody.mode) &&
        Objects.equals(this.arrivalPortCallId, transportRequestBody.arrivalPortCallId) &&
        Objects.equals(this.destinationPortCallId, transportRequestBody.destinationPortCallId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, vehicle, mode, arrivalPortCallId, destinationPortCallId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransportRequestBody {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    vehicle: ").append(toIndentedString(vehicle)).append("\n");
    sb.append("    mode: ").append(toIndentedString(mode)).append("\n");
    sb.append("    arrivalPortCallId: ").append(toIndentedString(arrivalPortCallId)).append("\n");
    sb.append("    destinationPortCallId: ").append(toIndentedString(destinationPortCallId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

