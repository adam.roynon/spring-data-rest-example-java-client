package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * AbstractJsonSchemaPropertyObject
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class AbstractJsonSchemaPropertyObject {

  @JsonProperty("title")
  private String title;

  @JsonProperty("readOnly")
  private Boolean readOnly;

  public AbstractJsonSchemaPropertyObject title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
  */
  
  @Schema(name = "title", required = false)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public AbstractJsonSchemaPropertyObject readOnly(Boolean readOnly) {
    this.readOnly = readOnly;
    return this;
  }

  /**
   * Get readOnly
   * @return readOnly
  */
  
  @Schema(name = "readOnly", required = false)
  public Boolean getReadOnly() {
    return readOnly;
  }

  public void setReadOnly(Boolean readOnly) {
    this.readOnly = readOnly;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AbstractJsonSchemaPropertyObject abstractJsonSchemaPropertyObject = (AbstractJsonSchemaPropertyObject) o;
    return Objects.equals(this.title, abstractJsonSchemaPropertyObject.title) &&
        Objects.equals(this.readOnly, abstractJsonSchemaPropertyObject.readOnly);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, readOnly);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AbstractJsonSchemaPropertyObject {\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    readOnly: ").append(toIndentedString(readOnly)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

