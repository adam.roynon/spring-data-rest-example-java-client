package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.beacon.customerservice.AbstractJsonSchemaPropertyObject;
import uk.co.beacon.customerservice.Item;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * JsonSchema
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class JsonSchema {

  @JsonProperty("title")
  private String title;

  @JsonProperty("description")
  private String description;

  @JsonProperty("properties")
  @Valid
  private Map<String, AbstractJsonSchemaPropertyObject> properties = null;

  @JsonProperty("requiredProperties")
  @Valid
  private List<String> requiredProperties = null;

  @JsonProperty("definitions")
  @Valid
  private Map<String, Item> definitions = null;

  @JsonProperty("type")
  private String type;

  @JsonProperty("$schema")
  private String $schema;

  public JsonSchema title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
  */
  
  @Schema(name = "title", required = false)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public JsonSchema description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  */
  
  @Schema(name = "description", required = false)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public JsonSchema properties(Map<String, AbstractJsonSchemaPropertyObject> properties) {
    this.properties = properties;
    return this;
  }

  public JsonSchema putPropertiesItem(String key, AbstractJsonSchemaPropertyObject propertiesItem) {
    if (this.properties == null) {
      this.properties = new HashMap<>();
    }
    this.properties.put(key, propertiesItem);
    return this;
  }

  /**
   * Get properties
   * @return properties
  */
  @Valid 
  @Schema(name = "properties", required = false)
  public Map<String, AbstractJsonSchemaPropertyObject> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, AbstractJsonSchemaPropertyObject> properties) {
    this.properties = properties;
  }

  public JsonSchema requiredProperties(List<String> requiredProperties) {
    this.requiredProperties = requiredProperties;
    return this;
  }

  public JsonSchema addRequiredPropertiesItem(String requiredPropertiesItem) {
    if (this.requiredProperties == null) {
      this.requiredProperties = new ArrayList<>();
    }
    this.requiredProperties.add(requiredPropertiesItem);
    return this;
  }

  /**
   * Get requiredProperties
   * @return requiredProperties
  */
  
  @Schema(name = "requiredProperties", required = false)
  public List<String> getRequiredProperties() {
    return requiredProperties;
  }

  public void setRequiredProperties(List<String> requiredProperties) {
    this.requiredProperties = requiredProperties;
  }

  public JsonSchema definitions(Map<String, Item> definitions) {
    this.definitions = definitions;
    return this;
  }

  public JsonSchema putDefinitionsItem(String key, Item definitionsItem) {
    if (this.definitions == null) {
      this.definitions = new HashMap<>();
    }
    this.definitions.put(key, definitionsItem);
    return this;
  }

  /**
   * Get definitions
   * @return definitions
  */
  @Valid 
  @Schema(name = "definitions", required = false)
  public Map<String, Item> getDefinitions() {
    return definitions;
  }

  public void setDefinitions(Map<String, Item> definitions) {
    this.definitions = definitions;
  }

  public JsonSchema type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  
  @Schema(name = "type", required = false)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public JsonSchema $schema(String $schema) {
    this.$schema = $schema;
    return this;
  }

  /**
   * Get $schema
   * @return $schema
  */
  
  @Schema(name = "$schema", required = false)
  public String get$Schema() {
    return $schema;
  }

  public void set$Schema(String $schema) {
    this.$schema = $schema;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JsonSchema jsonSchema = (JsonSchema) o;
    return Objects.equals(this.title, jsonSchema.title) &&
        Objects.equals(this.description, jsonSchema.description) &&
        Objects.equals(this.properties, jsonSchema.properties) &&
        Objects.equals(this.requiredProperties, jsonSchema.requiredProperties) &&
        Objects.equals(this.definitions, jsonSchema.definitions) &&
        Objects.equals(this.type, jsonSchema.type) &&
        Objects.equals(this.$schema, jsonSchema.$schema);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, description, properties, requiredProperties, definitions, type, $schema);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JsonSchema {\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
    sb.append("    requiredProperties: ").append(toIndentedString(requiredProperties)).append("\n");
    sb.append("    definitions: ").append(toIndentedString(definitions)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    $schema: ").append(toIndentedString($schema)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

