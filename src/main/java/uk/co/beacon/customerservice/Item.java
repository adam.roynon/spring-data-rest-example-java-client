package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.co.beacon.customerservice.AbstractJsonSchemaPropertyObject;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * Item
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class Item {

  @JsonProperty("type")
  private String type;

  @JsonProperty("properties")
  @Valid
  private Map<String, AbstractJsonSchemaPropertyObject> properties = null;

  @JsonProperty("requiredProperties")
  @Valid
  private List<String> requiredProperties = null;

  public Item type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  
  @Schema(name = "type", required = false)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Item properties(Map<String, AbstractJsonSchemaPropertyObject> properties) {
    this.properties = properties;
    return this;
  }

  public Item putPropertiesItem(String key, AbstractJsonSchemaPropertyObject propertiesItem) {
    if (this.properties == null) {
      this.properties = new HashMap<>();
    }
    this.properties.put(key, propertiesItem);
    return this;
  }

  /**
   * Get properties
   * @return properties
  */
  @Valid 
  @Schema(name = "properties", required = false)
  public Map<String, AbstractJsonSchemaPropertyObject> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, AbstractJsonSchemaPropertyObject> properties) {
    this.properties = properties;
  }

  public Item requiredProperties(List<String> requiredProperties) {
    this.requiredProperties = requiredProperties;
    return this;
  }

  public Item addRequiredPropertiesItem(String requiredPropertiesItem) {
    if (this.requiredProperties == null) {
      this.requiredProperties = new ArrayList<>();
    }
    this.requiredProperties.add(requiredPropertiesItem);
    return this;
  }

  /**
   * Get requiredProperties
   * @return requiredProperties
  */
  
  @Schema(name = "requiredProperties", required = false)
  public List<String> getRequiredProperties() {
    return requiredProperties;
  }

  public void setRequiredProperties(List<String> requiredProperties) {
    this.requiredProperties = requiredProperties;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Item item = (Item) o;
    return Objects.equals(this.type, item.type) &&
        Objects.equals(this.properties, item.properties) &&
        Objects.equals(this.requiredProperties, item.requiredProperties);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, properties, requiredProperties);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Item {\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
    sb.append("    requiredProperties: ").append(toIndentedString(requiredProperties)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

