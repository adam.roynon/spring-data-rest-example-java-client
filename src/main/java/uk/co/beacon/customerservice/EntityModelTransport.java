package uk.co.beacon.customerservice;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import uk.co.beacon.customerservice.Link;
import uk.co.beacon.customerservice.Vehicle;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * EntityModelTransport
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen")
public class EntityModelTransport {

  @JsonProperty("vehicle")
  private Vehicle vehicle;

  /**
   * Gets or Sets mode
   */
  public enum ModeEnum {
    OCEAN("OCEAN");

    private String value;

    ModeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ModeEnum fromValue(String value) {
      for (ModeEnum b : ModeEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("mode")
  private ModeEnum mode;

  @JsonProperty("arrivalPortCallId")
  private UUID arrivalPortCallId;

  @JsonProperty("destinationPortCallId")
  private UUID destinationPortCallId;

  @JsonProperty("_links")
  @Valid
  private Map<String, Link> links = null;

  public EntityModelTransport vehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
    return this;
  }

  /**
   * Get vehicle
   * @return vehicle
  */
  @Valid 
  @Schema(name = "vehicle", required = false)
  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public EntityModelTransport mode(ModeEnum mode) {
    this.mode = mode;
    return this;
  }

  /**
   * Get mode
   * @return mode
  */
  
  @Schema(name = "mode", required = false)
  public ModeEnum getMode() {
    return mode;
  }

  public void setMode(ModeEnum mode) {
    this.mode = mode;
  }

  public EntityModelTransport arrivalPortCallId(UUID arrivalPortCallId) {
    this.arrivalPortCallId = arrivalPortCallId;
    return this;
  }

  /**
   * Get arrivalPortCallId
   * @return arrivalPortCallId
  */
  @Valid 
  @Schema(name = "arrivalPortCallId", required = false)
  public UUID getArrivalPortCallId() {
    return arrivalPortCallId;
  }

  public void setArrivalPortCallId(UUID arrivalPortCallId) {
    this.arrivalPortCallId = arrivalPortCallId;
  }

  public EntityModelTransport destinationPortCallId(UUID destinationPortCallId) {
    this.destinationPortCallId = destinationPortCallId;
    return this;
  }

  /**
   * Get destinationPortCallId
   * @return destinationPortCallId
  */
  @Valid 
  @Schema(name = "destinationPortCallId", required = false)
  public UUID getDestinationPortCallId() {
    return destinationPortCallId;
  }

  public void setDestinationPortCallId(UUID destinationPortCallId) {
    this.destinationPortCallId = destinationPortCallId;
  }

  public EntityModelTransport links(Map<String, Link> links) {
    this.links = links;
    return this;
  }

  public EntityModelTransport putLinksItem(String key, Link linksItem) {
    if (this.links == null) {
      this.links = new HashMap<>();
    }
    this.links.put(key, linksItem);
    return this;
  }

  /**
   * Get links
   * @return links
  */
  @Valid 
  @Schema(name = "_links", required = false)
  public Map<String, Link> getLinks() {
    return links;
  }

  public void setLinks(Map<String, Link> links) {
    this.links = links;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EntityModelTransport entityModelTransport = (EntityModelTransport) o;
    return Objects.equals(this.vehicle, entityModelTransport.vehicle) &&
        Objects.equals(this.mode, entityModelTransport.mode) &&
        Objects.equals(this.arrivalPortCallId, entityModelTransport.arrivalPortCallId) &&
        Objects.equals(this.destinationPortCallId, entityModelTransport.destinationPortCallId) &&
        Objects.equals(this.links, entityModelTransport.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vehicle, mode, arrivalPortCallId, destinationPortCallId, links);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class EntityModelTransport {\n");
    sb.append("    vehicle: ").append(toIndentedString(vehicle)).append("\n");
    sb.append("    mode: ").append(toIndentedString(mode)).append("\n");
    sb.append("    arrivalPortCallId: ").append(toIndentedString(arrivalPortCallId)).append("\n");
    sb.append("    destinationPortCallId: ").append(toIndentedString(destinationPortCallId)).append("\n");
    sb.append("    links: ").append(toIndentedString(links)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

