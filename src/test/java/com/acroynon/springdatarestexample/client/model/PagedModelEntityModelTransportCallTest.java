/*
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.acroynon.springdatarestexample.client.model;

import com.acroynon.springdatarestexample.client.model.Link;
import com.acroynon.springdatarestexample.client.model.PageMetadata;
import com.acroynon.springdatarestexample.client.model.PagedModelEntityModelTransportCallEmbedded;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * Model tests for PagedModelEntityModelTransportCall
 */
public class PagedModelEntityModelTransportCallTest {
    private final PagedModelEntityModelTransportCall model = new PagedModelEntityModelTransportCall();

    /**
     * Model tests for PagedModelEntityModelTransportCall
     */
    @Test
    public void testPagedModelEntityModelTransportCall() {
        // TODO: test PagedModelEntityModelTransportCall
    }

    /**
     * Test the property 'embedded'
     */
    @Test
    public void embeddedTest() {
        // TODO: test embedded
    }

    /**
     * Test the property 'links'
     */
    @Test
    public void linksTest() {
        // TODO: test links
    }

    /**
     * Test the property 'page'
     */
    @Test
    public void pageTest() {
        // TODO: test page
    }

}
