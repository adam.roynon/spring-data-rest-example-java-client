/*
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package uk.co.beacon.customerservice.client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import uk.co.beacon.customerservice.client.model.Link;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * Model tests for EntityModelTransportCall
 */
public class EntityModelTransportCallTest {
    private final EntityModelTransportCall model = new EntityModelTransportCall();

    /**
     * Model tests for EntityModelTransportCall
     */
    @Test
    public void testEntityModelTransportCall() {
        // TODO: test EntityModelTransportCall
    }

    /**
     * Test the property 'locationId'
     */
    @Test
    public void locationIdTest() {
        // TODO: test locationId
    }

    /**
     * Test the property 'links'
     */
    @Test
    public void linksTest() {
        // TODO: test links
    }

}
