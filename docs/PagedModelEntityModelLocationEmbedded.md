

# PagedModelEntityModelLocationEmbedded


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**locations** | [**List&lt;EntityModelLocation&gt;**](EntityModelLocation.md) |  |  [optional] |



