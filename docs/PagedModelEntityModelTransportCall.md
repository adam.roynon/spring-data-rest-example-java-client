

# PagedModelEntityModelTransportCall


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**embedded** | [**PagedModelEntityModelTransportCallEmbedded**](PagedModelEntityModelTransportCallEmbedded.md) |  |  [optional] |
|**links** | [**Map&lt;String, Link&gt;**](Link.md) |  |  [optional] |
|**page** | [**PageMetadata**](PageMetadata.md) |  |  [optional] |



