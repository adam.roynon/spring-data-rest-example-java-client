

# PagedModelEntityModelTransport


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**embedded** | [**PagedModelEntityModelTransportEmbedded**](PagedModelEntityModelTransportEmbedded.md) |  |  [optional] |
|**links** | [**Map&lt;String, Link&gt;**](Link.md) |  |  [optional] |
|**page** | [**PageMetadata**](PageMetadata.md) |  |  [optional] |



