

# PagedModelEntityModelTransportEmbedded


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**transports** | [**List&lt;EntityModelTransport&gt;**](EntityModelTransport.md) |  |  [optional] |



