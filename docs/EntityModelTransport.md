

# EntityModelTransport


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**vehicle** | [**Vehicle**](Vehicle.md) |  |  [optional] |
|**mode** | [**ModeEnum**](#ModeEnum) |  |  [optional] |
|**arrivalPortCallId** | **UUID** |  |  [optional] |
|**destinationPortCallId** | **UUID** |  |  [optional] |
|**links** | [**Map&lt;String, Link&gt;**](Link.md) |  |  [optional] |



## Enum: ModeEnum

| Name | Value |
|---- | -----|
| OCEAN | &quot;OCEAN&quot; |



