# TransportEntityControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**deleteItemResourceTransportDelete**](TransportEntityControllerApi.md#deleteItemResourceTransportDelete) | **DELETE** /transports/{id} |  |
| [**getCollectionResourceTransportGet1**](TransportEntityControllerApi.md#getCollectionResourceTransportGet1) | **GET** /transports |  |
| [**getItemResourceTransportGet**](TransportEntityControllerApi.md#getItemResourceTransportGet) | **GET** /transports/{id} |  |
| [**patchItemResourceTransportPatch**](TransportEntityControllerApi.md#patchItemResourceTransportPatch) | **PATCH** /transports/{id} |  |
| [**postCollectionResourceTransportPost**](TransportEntityControllerApi.md#postCollectionResourceTransportPost) | **POST** /transports |  |
| [**putItemResourceTransportPut**](TransportEntityControllerApi.md#putItemResourceTransportPut) | **PUT** /transports/{id} |  |


<a name="deleteItemResourceTransportDelete"></a>
# **deleteItemResourceTransportDelete**
> deleteItemResourceTransportDelete(id)



delete-transport

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportEntityControllerApi apiInstance = new TransportEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      apiInstance.deleteItemResourceTransportDelete(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportEntityControllerApi#deleteItemResourceTransportDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **404** | Not Found |  -  |

<a name="getCollectionResourceTransportGet1"></a>
# **getCollectionResourceTransportGet1**
> PagedModelEntityModelTransport getCollectionResourceTransportGet1(page, size, sort)



get-transport

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportEntityControllerApi apiInstance = new TransportEntityControllerApi(defaultClient);
    Integer page = 0; // Integer | Zero-based page index (0..N)
    Integer size = 20; // Integer | The size of the page to be returned
    List<String> sort = Arrays.asList(); // List<String> | Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
    try {
      PagedModelEntityModelTransport result = apiInstance.getCollectionResourceTransportGet1(page, size, sort);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportEntityControllerApi#getCollectionResourceTransportGet1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **page** | **Integer**| Zero-based page index (0..N) | [optional] [default to 0] |
| **size** | **Integer**| The size of the page to be returned | [optional] [default to 20] |
| **sort** | [**List&lt;String&gt;**](String.md)| Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] |

### Return type

[**PagedModelEntityModelTransport**](PagedModelEntityModelTransport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json, application/x-spring-data-compact+json, text/uri-list

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

<a name="getItemResourceTransportGet"></a>
# **getItemResourceTransportGet**
> EntityModelTransport getItemResourceTransportGet(id)



get-transport

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportEntityControllerApi apiInstance = new TransportEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      EntityModelTransport result = apiInstance.getItemResourceTransportGet(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportEntityControllerApi#getItemResourceTransportGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

[**EntityModelTransport**](EntityModelTransport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |

<a name="patchItemResourceTransportPatch"></a>
# **patchItemResourceTransportPatch**
> EntityModelTransport patchItemResourceTransportPatch(id, transportRequestBody)



patch-transport

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportEntityControllerApi apiInstance = new TransportEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    TransportRequestBody transportRequestBody = new TransportRequestBody(); // TransportRequestBody | 
    try {
      EntityModelTransport result = apiInstance.patchItemResourceTransportPatch(id, transportRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportEntityControllerApi#patchItemResourceTransportPatch");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **transportRequestBody** | [**TransportRequestBody**](TransportRequestBody.md)|  | |

### Return type

[**EntityModelTransport**](EntityModelTransport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **204** | No Content |  -  |

<a name="postCollectionResourceTransportPost"></a>
# **postCollectionResourceTransportPost**
> EntityModelTransport postCollectionResourceTransportPost(transportRequestBody)



create-transport

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportEntityControllerApi apiInstance = new TransportEntityControllerApi(defaultClient);
    TransportRequestBody transportRequestBody = new TransportRequestBody(); // TransportRequestBody | 
    try {
      EntityModelTransport result = apiInstance.postCollectionResourceTransportPost(transportRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportEntityControllerApi#postCollectionResourceTransportPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **transportRequestBody** | [**TransportRequestBody**](TransportRequestBody.md)|  | |

### Return type

[**EntityModelTransport**](EntityModelTransport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |

<a name="putItemResourceTransportPut"></a>
# **putItemResourceTransportPut**
> EntityModelTransport putItemResourceTransportPut(id, transportRequestBody)



update-transport

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportEntityControllerApi apiInstance = new TransportEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    TransportRequestBody transportRequestBody = new TransportRequestBody(); // TransportRequestBody | 
    try {
      EntityModelTransport result = apiInstance.putItemResourceTransportPut(id, transportRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportEntityControllerApi#putItemResourceTransportPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **transportRequestBody** | [**TransportRequestBody**](TransportRequestBody.md)|  | |

### Return type

[**EntityModelTransport**](EntityModelTransport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **201** | Created |  -  |
| **204** | No Content |  -  |

