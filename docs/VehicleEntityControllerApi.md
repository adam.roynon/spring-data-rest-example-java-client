# VehicleEntityControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**deleteItemResourceVehicleDelete**](VehicleEntityControllerApi.md#deleteItemResourceVehicleDelete) | **DELETE** /vehicles/{id} |  |
| [**getCollectionResourceVehicleGet1**](VehicleEntityControllerApi.md#getCollectionResourceVehicleGet1) | **GET** /vehicles |  |
| [**getItemResourceVehicleGet**](VehicleEntityControllerApi.md#getItemResourceVehicleGet) | **GET** /vehicles/{id} |  |
| [**patchItemResourceVehiclePatch**](VehicleEntityControllerApi.md#patchItemResourceVehiclePatch) | **PATCH** /vehicles/{id} |  |
| [**postCollectionResourceVehiclePost**](VehicleEntityControllerApi.md#postCollectionResourceVehiclePost) | **POST** /vehicles |  |
| [**putItemResourceVehiclePut**](VehicleEntityControllerApi.md#putItemResourceVehiclePut) | **PUT** /vehicles/{id} |  |


<a name="deleteItemResourceVehicleDelete"></a>
# **deleteItemResourceVehicleDelete**
> deleteItemResourceVehicleDelete(id)



delete-vehicle

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleEntityControllerApi apiInstance = new VehicleEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      apiInstance.deleteItemResourceVehicleDelete(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleEntityControllerApi#deleteItemResourceVehicleDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **404** | Not Found |  -  |

<a name="getCollectionResourceVehicleGet1"></a>
# **getCollectionResourceVehicleGet1**
> PagedModelEntityModelVehicle getCollectionResourceVehicleGet1(page, size, sort)



get-vehicle

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleEntityControllerApi apiInstance = new VehicleEntityControllerApi(defaultClient);
    Integer page = 0; // Integer | Zero-based page index (0..N)
    Integer size = 20; // Integer | The size of the page to be returned
    List<String> sort = Arrays.asList(); // List<String> | Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
    try {
      PagedModelEntityModelVehicle result = apiInstance.getCollectionResourceVehicleGet1(page, size, sort);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleEntityControllerApi#getCollectionResourceVehicleGet1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **page** | **Integer**| Zero-based page index (0..N) | [optional] [default to 0] |
| **size** | **Integer**| The size of the page to be returned | [optional] [default to 20] |
| **sort** | [**List&lt;String&gt;**](String.md)| Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] |

### Return type

[**PagedModelEntityModelVehicle**](PagedModelEntityModelVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json, application/x-spring-data-compact+json, text/uri-list

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

<a name="getItemResourceVehicleGet"></a>
# **getItemResourceVehicleGet**
> EntityModelVehicle getItemResourceVehicleGet(id)



get-vehicle

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleEntityControllerApi apiInstance = new VehicleEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      EntityModelVehicle result = apiInstance.getItemResourceVehicleGet(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleEntityControllerApi#getItemResourceVehicleGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

[**EntityModelVehicle**](EntityModelVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |

<a name="patchItemResourceVehiclePatch"></a>
# **patchItemResourceVehiclePatch**
> EntityModelVehicle patchItemResourceVehiclePatch(id, vehicleRequestBody)



patch-vehicle

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleEntityControllerApi apiInstance = new VehicleEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    VehicleRequestBody vehicleRequestBody = new VehicleRequestBody(); // VehicleRequestBody | 
    try {
      EntityModelVehicle result = apiInstance.patchItemResourceVehiclePatch(id, vehicleRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleEntityControllerApi#patchItemResourceVehiclePatch");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **vehicleRequestBody** | [**VehicleRequestBody**](VehicleRequestBody.md)|  | |

### Return type

[**EntityModelVehicle**](EntityModelVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **204** | No Content |  -  |

<a name="postCollectionResourceVehiclePost"></a>
# **postCollectionResourceVehiclePost**
> EntityModelVehicle postCollectionResourceVehiclePost(vehicleRequestBody)



create-vehicle

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleEntityControllerApi apiInstance = new VehicleEntityControllerApi(defaultClient);
    VehicleRequestBody vehicleRequestBody = new VehicleRequestBody(); // VehicleRequestBody | 
    try {
      EntityModelVehicle result = apiInstance.postCollectionResourceVehiclePost(vehicleRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleEntityControllerApi#postCollectionResourceVehiclePost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **vehicleRequestBody** | [**VehicleRequestBody**](VehicleRequestBody.md)|  | |

### Return type

[**EntityModelVehicle**](EntityModelVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |

<a name="putItemResourceVehiclePut"></a>
# **putItemResourceVehiclePut**
> EntityModelVehicle putItemResourceVehiclePut(id, vehicleRequestBody)



update-vehicle

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleEntityControllerApi apiInstance = new VehicleEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    VehicleRequestBody vehicleRequestBody = new VehicleRequestBody(); // VehicleRequestBody | 
    try {
      EntityModelVehicle result = apiInstance.putItemResourceVehiclePut(id, vehicleRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleEntityControllerApi#putItemResourceVehiclePut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **vehicleRequestBody** | [**VehicleRequestBody**](VehicleRequestBody.md)|  | |

### Return type

[**EntityModelVehicle**](EntityModelVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **201** | Created |  -  |
| **204** | No Content |  -  |

