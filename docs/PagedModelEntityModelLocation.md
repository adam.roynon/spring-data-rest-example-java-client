

# PagedModelEntityModelLocation


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**embedded** | [**PagedModelEntityModelLocationEmbedded**](PagedModelEntityModelLocationEmbedded.md) |  |  [optional] |
|**links** | [**Map&lt;String, Link&gt;**](Link.md) |  |  [optional] |
|**page** | [**PageMetadata**](PageMetadata.md) |  |  [optional] |



