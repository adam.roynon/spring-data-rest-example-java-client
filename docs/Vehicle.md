

# Vehicle


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**id** | **UUID** |  |  [optional] |
|**imoNumber** | **String** |  |  [optional] |
|**name** | **String** |  |  [optional] |



