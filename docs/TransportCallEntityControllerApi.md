# TransportCallEntityControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**deleteItemResourceTransportcallDelete**](TransportCallEntityControllerApi.md#deleteItemResourceTransportcallDelete) | **DELETE** /transportCalls/{id} |  |
| [**getCollectionResourceTransportcallGet1**](TransportCallEntityControllerApi.md#getCollectionResourceTransportcallGet1) | **GET** /transportCalls |  |
| [**getItemResourceTransportcallGet**](TransportCallEntityControllerApi.md#getItemResourceTransportcallGet) | **GET** /transportCalls/{id} |  |
| [**patchItemResourceTransportcallPatch**](TransportCallEntityControllerApi.md#patchItemResourceTransportcallPatch) | **PATCH** /transportCalls/{id} |  |
| [**postCollectionResourceTransportcallPost**](TransportCallEntityControllerApi.md#postCollectionResourceTransportcallPost) | **POST** /transportCalls |  |
| [**putItemResourceTransportcallPut**](TransportCallEntityControllerApi.md#putItemResourceTransportcallPut) | **PUT** /transportCalls/{id} |  |


<a name="deleteItemResourceTransportcallDelete"></a>
# **deleteItemResourceTransportcallDelete**
> deleteItemResourceTransportcallDelete(id)



delete-transportcall

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportCallEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportCallEntityControllerApi apiInstance = new TransportCallEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      apiInstance.deleteItemResourceTransportcallDelete(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportCallEntityControllerApi#deleteItemResourceTransportcallDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **404** | Not Found |  -  |

<a name="getCollectionResourceTransportcallGet1"></a>
# **getCollectionResourceTransportcallGet1**
> PagedModelEntityModelTransportCall getCollectionResourceTransportcallGet1(page, size, sort)



get-transportcall

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportCallEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportCallEntityControllerApi apiInstance = new TransportCallEntityControllerApi(defaultClient);
    Integer page = 0; // Integer | Zero-based page index (0..N)
    Integer size = 20; // Integer | The size of the page to be returned
    List<String> sort = Arrays.asList(); // List<String> | Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
    try {
      PagedModelEntityModelTransportCall result = apiInstance.getCollectionResourceTransportcallGet1(page, size, sort);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportCallEntityControllerApi#getCollectionResourceTransportcallGet1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **page** | **Integer**| Zero-based page index (0..N) | [optional] [default to 0] |
| **size** | **Integer**| The size of the page to be returned | [optional] [default to 20] |
| **sort** | [**List&lt;String&gt;**](String.md)| Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] |

### Return type

[**PagedModelEntityModelTransportCall**](PagedModelEntityModelTransportCall.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json, application/x-spring-data-compact+json, text/uri-list

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

<a name="getItemResourceTransportcallGet"></a>
# **getItemResourceTransportcallGet**
> EntityModelTransportCall getItemResourceTransportcallGet(id)



get-transportcall

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportCallEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportCallEntityControllerApi apiInstance = new TransportCallEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      EntityModelTransportCall result = apiInstance.getItemResourceTransportcallGet(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportCallEntityControllerApi#getItemResourceTransportcallGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

[**EntityModelTransportCall**](EntityModelTransportCall.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |

<a name="patchItemResourceTransportcallPatch"></a>
# **patchItemResourceTransportcallPatch**
> EntityModelTransportCall patchItemResourceTransportcallPatch(id, transportCallRequestBody)



patch-transportcall

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportCallEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportCallEntityControllerApi apiInstance = new TransportCallEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    TransportCallRequestBody transportCallRequestBody = new TransportCallRequestBody(); // TransportCallRequestBody | 
    try {
      EntityModelTransportCall result = apiInstance.patchItemResourceTransportcallPatch(id, transportCallRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportCallEntityControllerApi#patchItemResourceTransportcallPatch");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **transportCallRequestBody** | [**TransportCallRequestBody**](TransportCallRequestBody.md)|  | |

### Return type

[**EntityModelTransportCall**](EntityModelTransportCall.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **204** | No Content |  -  |

<a name="postCollectionResourceTransportcallPost"></a>
# **postCollectionResourceTransportcallPost**
> EntityModelTransportCall postCollectionResourceTransportcallPost(transportCallRequestBody)



create-transportcall

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportCallEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportCallEntityControllerApi apiInstance = new TransportCallEntityControllerApi(defaultClient);
    TransportCallRequestBody transportCallRequestBody = new TransportCallRequestBody(); // TransportCallRequestBody | 
    try {
      EntityModelTransportCall result = apiInstance.postCollectionResourceTransportcallPost(transportCallRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportCallEntityControllerApi#postCollectionResourceTransportcallPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **transportCallRequestBody** | [**TransportCallRequestBody**](TransportCallRequestBody.md)|  | |

### Return type

[**EntityModelTransportCall**](EntityModelTransportCall.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |

<a name="putItemResourceTransportcallPut"></a>
# **putItemResourceTransportcallPut**
> EntityModelTransportCall putItemResourceTransportcallPut(id, transportCallRequestBody)



update-transportcall

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.TransportCallEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    TransportCallEntityControllerApi apiInstance = new TransportCallEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    TransportCallRequestBody transportCallRequestBody = new TransportCallRequestBody(); // TransportCallRequestBody | 
    try {
      EntityModelTransportCall result = apiInstance.putItemResourceTransportcallPut(id, transportCallRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TransportCallEntityControllerApi#putItemResourceTransportcallPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **transportCallRequestBody** | [**TransportCallRequestBody**](TransportCallRequestBody.md)|  | |

### Return type

[**EntityModelTransportCall**](EntityModelTransportCall.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **201** | Created |  -  |
| **204** | No Content |  -  |

