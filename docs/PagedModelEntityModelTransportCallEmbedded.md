

# PagedModelEntityModelTransportCallEmbedded


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**transportCalls** | [**List&lt;EntityModelTransportCall&gt;**](EntityModelTransportCall.md) |  |  [optional] |



