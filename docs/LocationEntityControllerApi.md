# LocationEntityControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**deleteItemResourceLocationDelete**](LocationEntityControllerApi.md#deleteItemResourceLocationDelete) | **DELETE** /locations/{id} |  |
| [**getCollectionResourceLocationGet1**](LocationEntityControllerApi.md#getCollectionResourceLocationGet1) | **GET** /locations |  |
| [**getItemResourceLocationGet**](LocationEntityControllerApi.md#getItemResourceLocationGet) | **GET** /locations/{id} |  |
| [**patchItemResourceLocationPatch**](LocationEntityControllerApi.md#patchItemResourceLocationPatch) | **PATCH** /locations/{id} |  |
| [**postCollectionResourceLocationPost**](LocationEntityControllerApi.md#postCollectionResourceLocationPost) | **POST** /locations |  |
| [**putItemResourceLocationPut**](LocationEntityControllerApi.md#putItemResourceLocationPut) | **PUT** /locations/{id} |  |


<a name="deleteItemResourceLocationDelete"></a>
# **deleteItemResourceLocationDelete**
> deleteItemResourceLocationDelete(id)



delete-location

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationEntityControllerApi apiInstance = new LocationEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      apiInstance.deleteItemResourceLocationDelete(id);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationEntityControllerApi#deleteItemResourceLocationDelete");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | No Content |  -  |
| **404** | Not Found |  -  |

<a name="getCollectionResourceLocationGet1"></a>
# **getCollectionResourceLocationGet1**
> PagedModelEntityModelLocation getCollectionResourceLocationGet1(page, size, sort)



get-location

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationEntityControllerApi apiInstance = new LocationEntityControllerApi(defaultClient);
    Integer page = 0; // Integer | Zero-based page index (0..N)
    Integer size = 20; // Integer | The size of the page to be returned
    List<String> sort = Arrays.asList(); // List<String> | Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported.
    try {
      PagedModelEntityModelLocation result = apiInstance.getCollectionResourceLocationGet1(page, size, sort);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationEntityControllerApi#getCollectionResourceLocationGet1");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **page** | **Integer**| Zero-based page index (0..N) | [optional] [default to 0] |
| **size** | **Integer**| The size of the page to be returned | [optional] [default to 20] |
| **sort** | [**List&lt;String&gt;**](String.md)| Sorting criteria in the format: property,(asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] |

### Return type

[**PagedModelEntityModelLocation**](PagedModelEntityModelLocation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json, application/x-spring-data-compact+json, text/uri-list

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

<a name="getItemResourceLocationGet"></a>
# **getItemResourceLocationGet**
> EntityModelLocation getItemResourceLocationGet(id)



get-location

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationEntityControllerApi apiInstance = new LocationEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    try {
      EntityModelLocation result = apiInstance.getItemResourceLocationGet(id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationEntityControllerApi#getItemResourceLocationGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |

### Return type

[**EntityModelLocation**](EntityModelLocation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |

<a name="patchItemResourceLocationPatch"></a>
# **patchItemResourceLocationPatch**
> EntityModelLocation patchItemResourceLocationPatch(id, locationRequestBody)



patch-location

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationEntityControllerApi apiInstance = new LocationEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    LocationRequestBody locationRequestBody = new LocationRequestBody(); // LocationRequestBody | 
    try {
      EntityModelLocation result = apiInstance.patchItemResourceLocationPatch(id, locationRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationEntityControllerApi#patchItemResourceLocationPatch");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **locationRequestBody** | [**LocationRequestBody**](LocationRequestBody.md)|  | |

### Return type

[**EntityModelLocation**](EntityModelLocation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **204** | No Content |  -  |

<a name="postCollectionResourceLocationPost"></a>
# **postCollectionResourceLocationPost**
> EntityModelLocation postCollectionResourceLocationPost(locationRequestBody)



create-location

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationEntityControllerApi apiInstance = new LocationEntityControllerApi(defaultClient);
    LocationRequestBody locationRequestBody = new LocationRequestBody(); // LocationRequestBody | 
    try {
      EntityModelLocation result = apiInstance.postCollectionResourceLocationPost(locationRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationEntityControllerApi#postCollectionResourceLocationPost");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **locationRequestBody** | [**LocationRequestBody**](LocationRequestBody.md)|  | |

### Return type

[**EntityModelLocation**](EntityModelLocation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Created |  -  |

<a name="putItemResourceLocationPut"></a>
# **putItemResourceLocationPut**
> EntityModelLocation putItemResourceLocationPut(id, locationRequestBody)



update-location

### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationEntityControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationEntityControllerApi apiInstance = new LocationEntityControllerApi(defaultClient);
    String id = "id_example"; // String | 
    LocationRequestBody locationRequestBody = new LocationRequestBody(); // LocationRequestBody | 
    try {
      EntityModelLocation result = apiInstance.putItemResourceLocationPut(id, locationRequestBody);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationEntityControllerApi#putItemResourceLocationPut");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **id** | **String**|  | |
| **locationRequestBody** | [**LocationRequestBody**](LocationRequestBody.md)|  | |

### Return type

[**EntityModelLocation**](EntityModelLocation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **201** | Created |  -  |
| **204** | No Content |  -  |

