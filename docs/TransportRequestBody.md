

# TransportRequestBody


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**id** | **UUID** |  |  [optional] |
|**vehicle** | [**Vehicle**](Vehicle.md) |  |  [optional] |
|**mode** | [**ModeEnum**](#ModeEnum) |  |  [optional] |
|**arrivalPortCallId** | **UUID** |  |  [optional] |
|**destinationPortCallId** | **UUID** |  |  [optional] |



## Enum: ModeEnum

| Name | Value |
|---- | -----|
| OCEAN | &quot;OCEAN&quot; |



