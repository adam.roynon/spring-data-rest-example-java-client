# VehicleSearchControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**executeSearchVehicleGet**](VehicleSearchControllerApi.md#executeSearchVehicleGet) | **GET** /vehicles/search/findByImoNumber |  |


<a name="executeSearchVehicleGet"></a>
# **executeSearchVehicleGet**
> EntityModelVehicle executeSearchVehicleGet(imoNumber)



### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.VehicleSearchControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    VehicleSearchControllerApi apiInstance = new VehicleSearchControllerApi(defaultClient);
    String imoNumber = "imoNumber_example"; // String | 
    try {
      EntityModelVehicle result = apiInstance.executeSearchVehicleGet(imoNumber);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling VehicleSearchControllerApi#executeSearchVehicleGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **imoNumber** | **String**|  | [optional] |

### Return type

[**EntityModelVehicle**](EntityModelVehicle.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |

