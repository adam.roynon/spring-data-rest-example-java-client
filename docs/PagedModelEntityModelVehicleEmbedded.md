

# PagedModelEntityModelVehicleEmbedded


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**vehicles** | [**List&lt;EntityModelVehicle&gt;**](EntityModelVehicle.md) |  |  [optional] |



