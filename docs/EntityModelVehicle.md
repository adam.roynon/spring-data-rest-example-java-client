

# EntityModelVehicle


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**imoNumber** | **String** |  |  [optional] |
|**name** | **String** |  |  [optional] |
|**links** | [**Map&lt;String, Link&gt;**](Link.md) |  |  [optional] |



