# LocationSearchControllerApi

All URIs are relative to *http://localhost:8080*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**executeSearchLocationGet**](LocationSearchControllerApi.md#executeSearchLocationGet) | **GET** /locations/search/findByName |  |


<a name="executeSearchLocationGet"></a>
# **executeSearchLocationGet**
> EntityModelLocation executeSearchLocationGet(name)



### Example
```java
// Import classes:
import com.acroynon.springdatarestexample.client.common.ApiClient;
import com.acroynon.springdatarestexample.client.common.ApiException;
import com.acroynon.springdatarestexample.client.common.Configuration;
import com.acroynon.springdatarestexample.client.common.models.*;
import com.acroynon.springdatarestexample.client.api.LocationSearchControllerApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8080");

    LocationSearchControllerApi apiInstance = new LocationSearchControllerApi(defaultClient);
    String name = "name_example"; // String | 
    try {
      EntityModelLocation result = apiInstance.executeSearchLocationGet(name);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling LocationSearchControllerApi#executeSearchLocationGet");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **name** | **String**|  | [optional] |

### Return type

[**EntityModelLocation**](EntityModelLocation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/hal+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **404** | Not Found |  -  |

